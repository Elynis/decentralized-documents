const App = artifacts.require("App");
const Authors = artifacts.require("Authors")

module.exports = function(deployer) {
  deployer.deploy(App)
  deployer.deploy(Authors)
};
