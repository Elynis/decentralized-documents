pragma solidity ^0.5.0;

import './Commit.sol';

contract Document{
    address[] private commits;
    string private title;

    constructor(string memory _title) public {
        title = _title;
    }

    function getCommits() public view returns(address[] memory) {
        return commits;
    }

    function getTitle() public view returns(string memory) {
        return title;
    }

    function setTitle(string memory _title) public {
        title = _title;
    }

    function pushCommit(address commit) public {
        commits.push(commit);
    }
}