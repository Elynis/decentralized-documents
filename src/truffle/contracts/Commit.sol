pragma solidity ^0.5.0;

import './Change.sol';

contract Commit {
    address[] private changes;
    address private author;
    uint256 creationDate;

    constructor() public {
        author = msg.sender;
        creationDate = now;
    }

    function getAuthor() public view returns(address) {
        return author;
    }

    function getChanges() public view returns(address[] memory) {
        return changes;
    }

    function getCreationDate() public view returns(uint256) {
        return creationDate;
    }

    function pushChange(int count, string memory modType, string memory value, int offset) public {
        address change = address(new Change(count, modType, value, offset));
        changes.push(change);
    }
}


