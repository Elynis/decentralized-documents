pragma solidity ^0.5.0;

contract Change{
    int count;
    string modType;
    string value;
    int offset;

    constructor(int _count, string memory _modType, string memory _value, int _offset) public {
        count = _count;
        modType = _modType;
        value = _value;
        offset = _offset;
    }

    function getCount() public view returns (int) {
        return count;
    }

    function getType() public view returns (string memory) {
        return modType;
    }

    function getValue() public view returns (string memory) {
        return value;
    }

    function getOffset() public view returns (int) {
        return offset;
    }
}