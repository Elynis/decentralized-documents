pragma solidity ^0.5.0;

import './Document.sol';

contract Collection {
    address[] private documents;
    string private name;

    constructor(string memory _name) public {
        name = _name;
    }

    function newDocument(string memory title) public returns (address) {
        address document = address(new Document(title));
        documents.push(document);
        return document;
    }

    function getName() public view returns(string memory) {
        return name;
    }

    function getDocuments() public view returns( address[] memory) {
        return documents;
    }
}


