pragma solidity ^0.5.0;

contract Authors{
    mapping(address => string) users;
    string lastNickname;

    function getNickname() public view returns (string memory) {
        return users[msg.sender];
    }

    function getNickname(address userAddress) public view returns (string memory) {
        return users[userAddress];
    }

    function register(string memory nickname) public {
        users[msg.sender] = nickname;
    }
}