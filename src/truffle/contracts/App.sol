pragma solidity ^0.5.0;

import './Collection.sol';

contract App{
    address[] private collections;

    function addCollection(string memory name) public returns (address) {
        address collection = address(new Collection(name));
        collections.push(collection);
        return collection;
    }

    function getCollections() public view returns (address[] memory) {
        return collections;
    }
}