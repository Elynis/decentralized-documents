const Collection = artifacts.require("Collection");
const App = artifacts.require("App");

contract("App", accounts => {

    it("Should return an empty array", () => {
        App.deployed()
        .then(app => app.getCollections.call())
        .then(collections => assert.equal(
            collections.lenght,
            0,
            "Not proper lenght of collections array"
        ))
    }),

    it("Should add new collection to empty collections array in App", () => {
        App.deployed()
        .then(app => {
            app.addCollection.call("Test collection")
            return app.getCollections.call()
        })
        .then(collections => assert.equal(
            collections.lenght,
            1,
            "Not proper lenght of collections array"
        ))
    })
})