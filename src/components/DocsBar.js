import React from 'react'

import { faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ItemsCarousel from 'react-items-carousel';

function DocsBar({ addDocument, documents, changeDocument, selectedDocument }) {

    if (selectedDocument == null) {
        selectedDocument = {id: 0}
    }

    var _docs = [(
        <div key={-1} className="docsbar-doc">
            <FontAwesomeIcon icon={faPlusCircle} className="docsbar-add-button" style={{fontSize: "3rem", marginTop: "4.5rem", color: "#34495e"}} onClick={() => addDocument("New document")}/>
        </div>
    )]

    documents.map((item, index) => _docs.push(
        <div key={item.id} className="docsbar-doc" onClick={() => changeDocument(item)}>
            <b>{item.title}</b>
        </div>
    ))

    return (
        <ItemsCarousel style={{ padding: "1rem", borderBottom: "1px solid gray", backgroundColor: "#efefef" }}
            // Carousel configurations
            numberOfCards={5}
            gutter={12}
            showSlither={true}
            firstAndLastGutter={true}
            freeScrolling={false}

            // Active item configurations
            //requestToChangeActive={changeDocument}
            activeItemIndex={selectedDocument.id}
            activePosition={'center'}

            chevronWidth={24}
            rightChevron={'>'}
            leftChevron={'<'}
            outsideChevron={false}
        >

            {_docs}
        </ItemsCarousel>
    )
}

export default DocsBar