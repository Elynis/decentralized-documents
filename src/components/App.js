import React, { Component } from "react";
import { Markup } from "interweave";
import { withSnackbar } from "notistack";

import CollectionBar from "./CollectionBar";
import DocsBar from "./DocsBar";
import Controls from "./Controls";
import Editor from "./PlainEditor";
import ToolBar from "./ToolBar";
import Audit from "./Audit";

import "../assets/css/App.css";
import AppInterface from "../truffle/build/contracts/App.json";
import CollectionInterface from "../truffle/build/contracts/Collection.json";
import DocumentInterface from "../truffle/build/contracts/Document.json";
import CommitInterface from "../truffle/build/contracts/Commit.json";
import ChangeInterface from "../truffle/build/contracts/Change.json";
import AuthorsInterface from "../truffle/build/contracts/Authors.json";

const BlockchainID = 1234;
const AppAddress = AppInterface.networks[BlockchainID].address;

class App extends Component {
  constructor(props) {
    super(props);

    console.log(props);

    this.state = {
      collections: [],
      documents: [],
      selectedCollection: null,
      selectedDocument: null,
      activeAccount: null,
      isAuditActive: false,
      activeCommitForAudit: null,
      preview: null
    };

    this.changeSelectedDocument = selectedDocument =>
      this.setState({ selectedDocument, activeCommitForAudit: null });

    this.loadChanges = this.loadChanges.bind(this);
    this.changeSelectedCollection = this.changeSelectedCollection.bind(this);
    this.addCollection = this.addCollection.bind(this);
    this.addDocument = this.addDocument.bind(this);
    this.onCommitClick = this.onCommitClick.bind(this);
    this.onAuditClick = this.onAuditClick.bind(this);
  }

  async componentDidMount() {
    //const account = await this.props.web3.eth.accounts.privateKeyToAccount("4054A9BAEFFA6679B3546B7C354332E1695D9A2D8F6A37BA1473EBD8050CF5FF")
    //this.props.web3.eth.accounts.wallet.add(account)
    //this.props.web3.eth.defaultAccount = account.address
    console.log(this.props);
    await this.setActiveAccount(0);
    await this.loadChanges();
  }

  async saveChanges() {
    console.time("saveChanges");
    this.props.enqueueSnackbar("Your document is saving ...", {
      variant: "info",
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "right"
      }
    });

    const document = this.state.selectedDocument;
    var commitABI = await new this.props.web3.eth.Contract(CommitInterface.abi);
    commitABI = await commitABI
      .deploy({ data: CommitInterface.bytecode })
      .send({
        from: this.state.activeAccount,
        gas: 1500000,
        gasPrice: "30000000000000"
      });

    const changes = this.getChanges(document);
    changes.forEach(async change => {
      await commitABI.methods
        .pushChange(change.count, change.type, change.value, change.offset)
        .send({
          from: this.state.activeAccount,
          gas: 1500000,
          gasPrice: "30000000000000"
        });
    });

    const documentABI = await new this.props.web3.eth.Contract(
      DocumentInterface.abi,
      document.address
    );
    await documentABI.methods.pushCommit(commitABI.options.address).send({
      from: this.state.activeAccount,
      gas: 1500000,
      gasPrice: "30000000000000"
    });
    await documentABI.methods.setTitle(document.title).send({
      from: this.state.activeAccount,
      gas: 1500000,
      gasPrice: "30000000000000"
    });

    await this.loadChanges();
    console.timeEnd("saveChanges");

    this.props.enqueueSnackbar("Your document is successfully saved", {
      variant: "success",
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "right"
      }
    });
  }

  async loadChanges(showSnack = false) {
    if (showSnack) {
      this.props.enqueueSnackbar("Your document is reloading ...", {
        variant: "info",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        }
      });
    }
    console.time("loadChanges");
    var selectedCollection = this.state.selectedCollection;
    var documents = [];
    var selectedDocument = this.state.selectedDocument;
    var activeCommitForAudit = null;

    const collections = await this.loadCollections();
    if (collections.length > 0) {
      if (selectedCollection == null) {
        selectedCollection = collections[0];
      }

      documents = await this.loadDocuments(selectedCollection);
      if (documents.length > 0) {
        var id = 0;
        if (selectedDocument != null) {
          id = selectedDocument.id;
        }
        selectedDocument = documents[id];

        const commit =
          selectedDocument.commits[selectedDocument.commits.length - 1];
        if (commit != null) {
          activeCommitForAudit = commit.id;
        }
      }
    }
    console.timeEnd("loadChanges");

    if (showSnack) {
      this.props.enqueueSnackbar("Your document is successfully reloaded", {
        variant: "success",
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "right"
        }
      });
    }
    this.setState({
      collections,
      selectedCollection,
      documents,
      selectedDocument
    });

    if (activeCommitForAudit != null) {
      this.onCommitClick(activeCommitForAudit);
    }
  }

  getChanges(document) {
    const prev = document.prevContent;
    const curr = document.currentContent;
    console.log("getChanges prev");
    console.log(document.prevContent);
    console.log("getChanges curr");
    console.log(document.currentContent);
    var Diff = require("diff");

    var changes = [];
    var diffLines = Diff.diffLines(prev, curr);
    var linesIndex = 0;
    diffLines.forEach(function(part) {
      var n_lines = part.count;
      // Jeżeli teskt nie jest ani dodany, ani usunięty to należy jedynie przesunąć offset indeksu obecnej linii
      if (
        typeof part.added === "undefined" &&
        typeof part.removed === "undefined"
      ) {
        linesIndex += n_lines;
      } else {
        // Jeśli tekst faktycznie jest inny to należy ustawić gdzie rozpoczyna się różnica
        part.offset = linesIndex;
        part.type = part.added ? "ADD" : "REMOVE";
        changes.push(part);
      }
    });
    return changes;
  }

  changeSelectedCollection(selectedCollection) {
    this.loadDocuments(selectedCollection).then(documents => {
      this.setState({ selectedCollection, documents });
    });
  }

  async setActiveAccount(number) {
    const accounts = await this.props.web3.eth.getAccounts();
    this.setState({ activeAccount: accounts[number] });
  }

  async loadCollections() {
    const appABI = await new this.props.web3.eth.Contract(
      AppInterface.abi,
      AppAddress
    );

    const collectionsAddresses = await appABI.methods
      .getCollections()
      .call({ from: this.state.activeAccount });

    console.log(collectionsAddresses);

    const collections = await this.resolveCollections(collectionsAddresses);
    return collections;
  }

  resolveCollections(collectionsAddresses) {
    return Promise.all(
      collectionsAddresses.map(async (item, index) => {
        const collectionABI = await new this.props.web3.eth.Contract(
          CollectionInterface.abi,
          item
        );
        const name = await collectionABI.methods
          .getName()
          .call({ from: this.state.activeAccount });
        return { id: index, name: name, address: item };
      })
    );
  }

  async loadDocuments(collection) {
    const collectionABI = await new this.props.web3.eth.Contract(
      CollectionInterface.abi,
      collection.address
    );
    const documentsAddresses = await collectionABI.methods
      .getDocuments()
      .call({ from: this.state.activeAccount });
    const documents = await this.resolveDocuments(documentsAddresses);
    for (var i = 0; i < documents.length; i++) {
      const document = documents[i];
      const documentABI = await new this.props.web3.eth.Contract(
        DocumentInterface.abi,
        document.address
      );
      const commitsAddresses = await documentABI.methods
        .getCommits()
        .call({ from: this.state.activeAccount });
      document.commits = await this.resolveCommits(commitsAddresses);
      for (var j = 0; j < document.commits.length; j++) {
        const commit = document.commits[j];
        commit.changes = await this.resolveChanges(commit.changesAddresses);
        this.applyChanges(commit.changes, document);
        if (document.title === "1") console.log(commit.changes);
        document.prevContent = document.currentContent;
        // Dodać linię niżej TODO:
      }
    }
    return documents;
  }

  resolveCommits(commitsAddresses) {
    return Promise.all(
      commitsAddresses.map(async (item, index) => {
        const commitABI = await new this.props.web3.eth.Contract(
          CommitInterface.abi,
          item
        );
        const changesAddresses = await commitABI.methods
          .getChanges()
          .call({ from: this.state.activeAccount });
        const creationDate = await commitABI.methods
          .getCreationDate()
          .call({ from: this.state.activeAccount });
        const authorAddress = await commitABI.methods
          .getAuthor()
          .call({ from: this.state.activeAccount });
        const authorsABI = await new this.props.web3.eth.Contract(
          AuthorsInterface.abi,
          AuthorsInterface.networks[BlockchainID].address
        );
        const authorNickname = await authorsABI.methods
          .getNickname(authorAddress)
          .call({ from: this.state.activeAccount });
        return {
          id: index,
          changesAddresses,
          creationDate,
          authorAddress,
          authorNickname
        };
      })
    );
  }

  resolveChanges(changesAddresses) {
    return Promise.all(
      changesAddresses.map(async (item, index) => {
        const change = await new this.props.web3.eth.Contract(
          ChangeInterface.abi,
          item
        );
        const count = (
          await change.methods
            .getCount()
            .call({ from: this.state.activeAccount })
        ).toNumber();
        const type = await change.methods
          .getType()
          .call({ from: this.state.activeAccount });
        const value = await change.methods
          .getValue()
          .call({ from: this.state.activeAccount });
        const offset = (
          await change.methods
            .getOffset()
            .call({ from: this.state.activeAccount })
        ).toNumber();
        return { id: index, count, type, value, offset };
      })
    );
  }

  resolveDocuments(documentsAddresses) {
    return Promise.all(
      documentsAddresses.map(async (item, index) => {
        const documentABI = await new this.props.web3.eth.Contract(
          DocumentInterface.abi,
          item
        );
        const title = await documentABI.methods
          .getTitle()
          .call({ from: this.state.activeAccount });
        return {
          id: index,
          title,
          address: item,
          prevContent: "",
          currentContent: ""
        };
      })
    );
  }

  applyChanges(changes, document) {
    var content = document.currentContent;
    changes.forEach(change => {
      if (change.type == "ADD") content = this.addLine(content, change);
      else if (change.type == "REMOVE")
        content = this.removeLine(content, change);
      else throw "Unsupported change type: " + change.type;
    });
    document.currentContent = content;
  }

  addLine(text, change) {
    var lines = this.splitTextToLines(text);
    if (lines.length == 0) return change.value;

    lines.splice(change.offset, 0, change.value);
    text = lines.join("");
    return text;
  }

  removeLine(text, change) {
    var lines = this.splitTextToLines(text);
    if (lines.length == 0) return "";

    lines.splice(change.offset, change.count);
    text = lines.join("");
    return text;
  }

  splitTextToLines(text) {
    var lines = text.split("\n");
    lines = lines.map(value => {
      value = value.replace("\r", "");
      value += "\n";
      return value;
    });
    lines[lines.length - 1] = lines[lines.length - 1].trimRight();
    return lines;
  }

  async addCollection(name) {
    console.time("addCollection");
    const app = await new this.props.web3.eth.Contract(
      AppInterface.abi,
      AppAddress
    );
    await app.methods.addCollection(name).send({
      from: this.state.activeAccount,
      gas: 1500000,
      gasPrice: "30000000000000"
    });
    const collections = await this.loadCollections();
    console.timeEnd("addCollection");
    this.setState({ collections });
  }

  async addDocument(name) {
    console.time("addDocument");

    const collection = this.state.selectedCollection;
    if (collection == null) {
      return;
    }

    const collectionABI = await new this.props.web3.eth.Contract(
      CollectionInterface.abi,
      collection.address
    );
    await collectionABI.methods.newDocument(name).send({
      from: this.state.activeAccount,
      gas: 1500000,
      gasPrice: "30000000000000"
    });

    this.loadDocuments(collection).then(documents => {
      this.setState({ documents, selectedCollection: collection });
      console.timeEnd("addDocument");
    });
  }

  onCommitClick(id) {
    const mockDoc = { prevContent: "", currentContent: "" };
    for (var i = 0; i <= id; i++) {
      mockDoc.prevContent = mockDoc.currentContent;
      this.applyChanges(
        this.state.selectedDocument.commits[i].changes,
        mockDoc
      );
    }
    this.setState({ activeCommitForAudit: id, preview: mockDoc });
  }

  onAuditClick() {
    const document = this.state.selectedDocument;
    const commit = document.commits[document.commits.length - 1];
    this.setState({
      isAuditActive: !this.state.isAuditActive,
      activeCommitForAudit: null,
      preview: null
    });
    if (commit != null) {
      this.onCommitClick(commit.id);
    }
  }

  render() {
    const document = this.state.isAuditActive ? (
      <Audit
        document={this.state.selectedDocument}
        preview={this.state.preview}
        activeCommit={this.state.activeCommitForAudit}
        onCommitClick={id => this.onCommitClick(id)}
      />
    ) : (
      <Editor
        document={this.state.selectedDocument}
        changeSelectedDocument={this.changeSelectedDocument}
      />
    );
    return (
      <div
        style={{
          height: "-webkit-fill-available",
          flexDirection: "column",
          display: "flex"
        }}
      >
        <div
          style={{
            "-webkit-app-region": "drag",
            position: "absolute",
            height: "35px",
            width: "100vw",
            top: 0,
            zIndex: 1000,
            left: 0
          }}
        ></div>

        <CollectionBar
          selectedCollection={this.state.selectedCollection}
          collections={this.state.collections}
          addCollection={this.addCollection}
          changeCollection={this.changeSelectedCollection}
        />
        <Controls
          reloadDocFun={() => this.loadChanges(true)}
          saveDocFun={() => this.saveChanges()}
          logoutFun={this.props.logoutFun}
        />
        <DocsBar
          selectedDocument={this.state.selectedDocument}
          documents={this.state.documents}
          addDocument={this.addDocument}
          changeDocument={this.changeSelectedDocument}
        />
        <ToolBar
          document={this.state.selectedDocument}
          changeSelectedDocument={this.changeSelectedDocument}
          isAuditActive={this.state.isAuditActive}
          onAuditClick={this.onAuditClick}
        />
        {document}
      </div>
    );
  }
}

export default withSnackbar(App);
