import React, {Component} from "react"
import App from './App'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Authors from '../truffle/build/contracts/Authors'

const Web3 = require('web3')
var HDWalletProvider = require("truffle-hdwallet-provider")

class Login extends Component {
    constructor() {
        super()
        this.state = {
            privateKey: localStorage.getItem("privateKey") || "",
            url: localStorage.getItem("url") || "",
            nickname: "",
            web3: null,
            isLogged: false,
            isNewUser: false,
            isLoading: false,
        }
        this.handleChange = this.handleChange.bind(this)
        this.onLoginFormSubmit = this.onLoginFormSubmit.bind(this)
        this.onNewUserSubmit = this.onNewUserSubmit.bind(this)
        this.onLogout = this.onLogout.bind(this)
    }

    handleChange(event) {
        const key = event.target.id
        this.setState({[key]: event.target.value})
    }

    componentDidMount() {
        if (this.state.privateKey != "" && this.state.url != "") {
            const web3 = new Web3(new HDWalletProvider(this.state.privateKey, this.state.url), null, { transactionConfirmationBlocks: 1 })
            //const web3 = new Web3(this.state.url)
            this.isNewUser(web3).then(isNewUser => {
                if (isNewUser) this.setState({isNewUser: true, web3, isLoading: false})
                else this.setState({isLogged: true, web3, isLoading: false})
            })

            this.setState({isLoading: true})
        }
    }

    onLoginFormSubmit(event) {
        event.preventDefault()
        
        const web3 = new Web3(new HDWalletProvider(this.state.privateKey, this.state.url), null, { transactionConfirmationBlocks: 1 })
        //const web3 = new Web3(this.state.url) // Your ganache IP address goes here

        this.isNewUser(web3).then(isNewUser => {
            localStorage.setItem("privateKey", this.state.privateKey)
            localStorage.setItem("url", this.state.url)
            if (isNewUser) this.setState({isNewUser: true, web3, isLoading: false})
            else this.setState({isLogged: true, web3, isLoading: false})
        })

        this.setState({isLoading: true})
    }

    async isNewUser(web3) {
        const authorsABI = await new web3.eth.Contract(Authors.abi, Authors.networks[1234].address)
        console.log(authorsABI)
        const accounts = await web3.eth.getAccounts()
        console.log(accounts)
        const nickname = await authorsABI.methods.getNickname().call({ from: accounts[0] })

        console.log(nickname)

        if (nickname == null || nickname === "") {
            return true
        }
        return false
    }

    onNewUserSubmit(event) {
        event.preventDefault()
        if (this.state.nickname != "") {
            this.register(this.state.web3, this.state.nickname).then(() => {
                this.setState({isLogged: true, isLoading: false})
            })
        }

        this.setState({isLoading: true})
    }
    
    async register(web3, nickname) {
        console.log(web3)
        const authorsABI = await new web3.eth.Contract(Authors.abi, Authors.networks[1234].address);
        
        console.log(authorsABI)
        const accounts = await web3.eth.getAccounts()

        await authorsABI.methods.register(nickname).send({from: accounts[0], gas: 1500000, gasPrice: '30000000000000' })
    }

    onLogout() {
        this.setState({ 
            isLogged: false,
            isNewUser: false,
            isLoading: false
         })
         localStorage.setItem("privateKey", "")
        localStorage.setItem("url", "")
    } 
    
    render() {
        if(this.state.isLogged){
            console.log("Tu powinien być web3")
            console.log(this.state.web3)
            return <App logoutFun={this.onLogout} web3={this.state.web3}/>
        }

        const LoginForm = (
            <form onSubmit={this.onLoginFormSubmit}>
                <div className="mb-5">
                <h1>Welcome in decentralized documents</h1>
                <h3>Log in to ethereumn network to continue</h3>
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" id="url" aria-describedby="urlHelp" placeholder="Enter URL RPC Address" onChange={this.handleChange} value={this.state.url}/>
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" id="privateKey" aria-describedby="privateKeyHelp" placeholder="Enter your PrivateKey or 12 words menmonic" onChange={this.handleChange} value={this.state.privateKey}/>
                    <small id="privateKeyHelp" className="form-text text-muted">We'll never share your private keys with anyone else.</small>
                </div>
                
                <Button style={{display: "block", marginLeft: "auto", marginRight: "auto", width: "60%", marginTop: "3rem"}} variant="contained" color="primary" type="submit">Log in</Button>
          </form>
        )

        const Loading = <CircularProgress />

        const NewUser = (
            <form onSubmit={this.onNewUserSubmit}>
                <div className="mb-5">
                <h1>It seems that you are new here</h1>
                <h3>Introduce yourself</h3>
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" id="nickname" aria-describedby="urlHelp" placeholder="Enter nickname" onChange={this.handleChange} value={this.state.nickname}/>
                </div>
                
                <Button style={{display: "block", marginLeft: "auto", marginRight: "auto", width: "60%", marginTop: "3rem"}} variant="contained" color="primary" type="submit">Go to decentralized documents</Button>
            </form>
        )

        var render = LoginForm
        if (this.state.isNewUser) render = NewUser
        if (this.state.isLoading) render = Loading
        
        return (
            <div>
                <div style={{"-webkit-app-region": "drag", position: 'absolute', height: '35px', width: '100vw', top:0, zIndex: 1000, left:0}}></div>
                <div style={{position: "absolute", left: "50%", top: "50%", transform: "translate(-50%, -50%)"}}>
                    {render}
                </div>
            </div>
        )
    }
}

export default Login