import React, { Component } from 'react'
import ReactDiffViewer from 'react-diff-viewer'

class MergeCodeTextArea extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        if(this.props.document == null) {
            return null
        }
        return (
            <ReactDiffViewer
                oldValue={this.props.document.prevContent}
                newValue={this.props.document.currentContent}
                splitView={false}
                showDiffOnly={false}
                disableWordDiff={true}
            />
        )
    }

}

export default MergeCodeTextArea