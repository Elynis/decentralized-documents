import React, { Component } from 'react'
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class CollectionBar extends Component {
    constructor(props){
        super(props)

        this.state = {
            displayIcon: true,
            newCollectionName: "",
            inputRef: null
        }

        this.handleChange = (event) => this.setState({ newCollectionName: event.target.value })
        this.setDisplayIcon = (state) => this.setState({ displayIcon: state })

        this.handleKey = this.handleKey.bind(this)
    }

    componentDidUpdate() {
        if (!this.state.displayIcon && this.state.inputRef != null) {
            this.state.inputRef.focus()
        }
    }

    handleKey(event) {
        switch(event.key) {
            case 'Enter':
                this.props.addCollection(this.state.newCollectionName)
                this.setState({ displayIcon: true, newCollectionName: "" })
        }
    }

    render() {
        var collectionButtons = this.props.collections.map((item, _) => {
            var divClass = "collectionbar-button"
            if(item === this.props.selectedCollection) {
                divClass += " collectionbar-button-chosen"
            }
            return (<div key={item.id} className={divClass} onClick={() => this.props.changeCollection(item)}><b>{item.name}</b></div>)
        })

        var addButton = this.state.displayIcon ? 
            (<FontAwesomeIcon icon={faPlusCircle} className="collection-add-button" onClick={() => this.setDisplayIcon(false)}/>)
            :
            (<div style={{margin: "20px"}}>
                <input ref={(input) => this.state.inputRef = input} className="collection-add-field form-control" value={this.state.newCollectionName} onKeyDown={this.handleKey} onChange={this.handleChange} />
            </div>)
    
        return (
            <div className="collectionBar">
                {collectionButtons}
                {addButton}
            </div>
        )
    }
}

export default CollectionBar