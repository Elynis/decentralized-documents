import React from 'react'
import { faSync, faSave, faCircle, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Controls({ saveDocFun, reloadDocFun, logoutFun }) {
    return (
        <div className="controls">
            <div style={{ textAlign: 'center' }} className="control" onClick={saveDocFun}>
                <FontAwesomeIcon icon={faSave} />
                <h6>Save</h6>
            </div>
            <div style={{ textAlign: 'center', borderLeft: '1px solid #1d6391' }} className="control" onClick={reloadDocFun}>
                <FontAwesomeIcon style={{color: "#e74c3c", position: "absolute", transform: "translate(50%, -50%)", visibility: "hidden"}} icon={faCircle} />
                <FontAwesomeIcon icon={faSync} />
                <h6>Reload</h6>
            </div>
            <div style={{ textAlign: 'center', borderLeft: '1px solid #1d6391' }} className="control" onClick={logoutFun}>
                <FontAwesomeIcon icon={faSignOutAlt} />
                <h6>Logout</h6>
            </div>
        </div>
    )
}

export default Controls