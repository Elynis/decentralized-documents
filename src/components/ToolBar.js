import React from 'react'
import { faFileInvoice, faFile } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function ToolBar({document, changeSelectedDocument, isAuditActive, onAuditClick}) {

    if (document == null) {
        return null
    }

    const iconStyle = {fontSize: "2rem", color: "#34495e", height: "50px", padding: "10px", width: "50px"}
    if (isAuditActive){
        iconStyle.flex = "1 200px"
        iconStyle.background = "#e84393"
        iconStyle.color = "white"
    }

    const handleTitleChange = (event) => {
        document.title = event.target.value
        changeSelectedDocument(document)
    }

    return (
        <div style={{display: "flex"}}>
            <input style={{flex: 8, fontSize: "2rem", paddingLeft: "1rem", border: 0}} onChange={handleTitleChange} placeholder="Title" value={document.title} />
            <FontAwesomeIcon icon={faFileInvoice} style={iconStyle} onClick={onAuditClick}/>
        </div>
    )
}

export default ToolBar