import React, { Component } from 'react'
import MergeCodeTextArea from './MergeCodeTextArea';

function Audit({document, preview, activeCommit, onCommitClick}) {
    if(document == null || document.commits == null) return null
    
    const commits = document.commits.slice(0).reverse().map((item, _) => {
        const style = {textAlign: "center", padding: "10px"}
        if (item.id == activeCommit) {
            style.background = "#fd79a8"
        }

        return(
            <div key={item.id} style={style} onClick={() => onCommitClick(item.id)}>{(new Date(item.creationDate.toNumber()*1000)).toLocaleString()}
            <br/>
            {item.authorNickname}
            </div>
            )
        })

    return (
        <div style={{flexDirection: "row", display: "flex", flex: 1}}>
        <div style={{flex: 8}}>
            <div style={{paddingTop: "20px"}}>
                <MergeCodeTextArea document={preview}/>
            </div>
        </div>
        <div style={{background: "#e84393", flex: "1 198px"}}>
            <div style={{textAlign: "center", fontSize: "1.5rem", margin: "10px"}}>Commits History</div>
            {commits}
        </div>
        
    </div>
    )
}

export default Audit