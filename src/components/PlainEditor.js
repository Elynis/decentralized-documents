import React from 'react'
import TextareaAutosize from 'react-textarea-autosize';

function PlainEditor ({document, changeSelectedDocument}) {

    if (document == null){
        return null
    }

    var onChange = (event) => {
        document.currentContent = event.target.value
        changeSelectedDocument(document)
    }

    return (
        <TextareaAutosize minRows={20} className="form-control rounded-0" onChange={onChange} value={document.currentContent}/>
    )
}

export default PlainEditor