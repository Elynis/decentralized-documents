
Uwagi:

_Umieszczać dużo obrazków i diagramów_
_Piszemy z punktu widzenia osoby z zerowym autorytetem tzn. jak coś piszemy to zawsze z dopiskiem według kogo_

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">

# TODO:
 <!-- - opisać zawartość tabeli na str. 15  -->
 <!-- - na str. 16 dodać opis, dlaczego wybraliśmy architekturę przedstawioną na obrazku -->
 - omówić przypadki użycia ???
 <!-- - uzasadnić dlaczego zaproponowaliśmy taki diagram klas  -->
 <!-- - opis węzła zmienić na listing i dać jego odniesienie w tekscie -->
 - dodać opis kluczowych linii z opisu węzła - co one robią i w jakim celu ????
 <!-- - opisać lepiej autoryzacja w rodziale 4.2 -->


Spis treści:

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">

- Wstęp => Michał, Król piśmiennictwa, arcyseksiak literatury

  - Z czego wynika problem, który chcemy rozwiązać
  - Co nasza aplikacja rozwiązuje?
  - Zastosowanie
  - Dlaczego inne rozwiązania go nie rozwiązują
  - Rozdział Blockchain (kwestia sporna)



<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">


- Przegląd aktualnych rozwiązań => Szymon "No siema"

  - Po tytule wydaje mi się, że ta praca wspiera nasze trzymanie on-chain. Może nam uratować tyłek.
  https://patentimages.storage.googleapis.com/de/fe/6a/c8b1976a0f3009/US20170364701A1.pdf

  Autor proponuje rozwiązanie: 

  Ponadto w wielu przypadkach osoba atakująca może być poświadczonym podmiotem w sieci, takim jak nieuczciwy pracownik, co sprawia, że wiele tradycyjnych podejść do bezpieczeństwa magazynu danych jest w niektórych przypadkach niewystarczające. Zwiększając ryzyko, w wielu przypadkach osoba atakująca może próbować zamaskować swoją aktywność w sieci, usuwając dzienniki dostępu przechowywane w magazynach danych.

  - Wydaje mi się, że bardzo fajny papier. Zawiera kilka obrazków opisujących rozrost Blockchaina przy koszyrstaniu z nasego onchainowego zapisywania
  https://iopscience.iop.org/article/10.1088/1757-899X/561/1/012117/pdf

  - MIStore - Jakiś dziwny system wymiany pomiędzy szpitalami, a ubezpieczalniami. Dodaje bo mają fajnie opisane problemu Ethereum na stronie 9. Nie wiem czy sie przyda.
  https://link.springer.com/content/pdf/10.1007%2Fs10916-018-0996-4.pdf

  - Aplikacja na Android, która symuluję detekcję wypadku samochodowego i z pomocą Bitcoina i hashu pliku nagrania potwierdza niezmienność nagrania (fajnie tłumaczą w akapicie 3.5)
  https://pdfs.semanticscholar.org/62c7/e27882758a1088ed8332ed166392141b0b68.pdf

  - Jakiś patent Google, który jedynie co to mnie wpienia, że go wcześniej nie przeczytaliśmy. Ogólnie opowiada o utopijnej architekturze naszego rozwiązania. Można wykorzystać to i powiedzieć jedynie że wydaje nam się, że da się to zrobić inaczej.
  https://patentimages.storage.googleapis.com/88/b4/cc/631631b28e99d7/US20180025181A1.pdf

  
  - Opowiadają o praktycznie naszej aplikacji... Jedynie mówią, że wykorzystywać się to będzie do projektów konstrukcyjnych... 
  https://reader.elsevier.com/reader/sd/pii/S187770581733179X?token=714FA6A2B5EA4A19171D49B3BACD45F941ED602CFDB44D86EEFFA38AADEA0E3A3AB12D43D3DF6FDB11042ED19D9C2AEC

  - MedRec
  https://pdfs.semanticscholar.org/56e6/5b469cad2f3ebd560b3a10e7346780f4ab0a.pdf

  - BigchainDB, mogliśmy z tego skorzystać...
  https://www.bigchaindb.com/
  https://www.bigchaindb.com/whitepaper/bigchaindb-whitepaper.pdf
  
  - Też w jakimś stopniu nasza praca, ale bardzie to powtarza ciągle o bezpieczeństwie rozwiązania
  http://www.jmdet.com/wp-content/uploads/2019/02/5jmdet_12_2_10-1.pdf

  - Git. Cechy gita do porównania:
    - Squashowanie commitów
    - można zmieniać autorów
    - Brak narzędzia GUI do obsługi (?)
    - Prędkość wprowadzania zmian
    - Branche
    - Lokalne commity
  
  - Tutaj trzeba przeredagować to co mamy już w dokumencie:
    - Bitcoin
    - Ethereum
    
  - Czy dodajemy jakieś przykłady z Dapp.com ?
    - Arcane Docs (dokumenty przechowywane w jakieś chmurze opartej o blockchain)
    https://www.dapp.com/dapp/arcane-docs

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">

- Opis Aplikacji

  - Decyzje projektowe
    (Podjeliśmy decyzję, że spróbujemy podejścia onchain dla trzymania WSZYSTKICH danych aplikacji. Aplikacja będzie odpowiedzialna za wyświetlania, modyfikowanie dokumentów. Sieć blockchain korzystać będzie z inteligentych kontraktów platformy Etehreum, aby przechowywać wszystkie dane ) 
  - Opis UI
    (Rozszerzyć o to co się dzieje w kodzie na poszczególnych akcjach)
  - Kontrakty
    (Zrobione)
  - Usunąć algorytm składający zmiany
    (Czy aby na pewno? Ja bym nie usuwał)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">

- Testy => Mateusz, Mistrz-Kozak Getha, Zdobywca węzłów

  - Zamienić scenariusze testowe na diagramy
  - Dorobić wiele testów na różnych liczbach węzłów
  - Zrobić wykresy do testów
  - Zrobić kilku węzłową sieć P2P i pokazać jak tam działa program. 
    (obecnie działa kozacko)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">

- Wnioski
  - Napisać, że podjęte decyzje projektowe nie były najlepsze
    (Były turbo średnie)
  - Co według nas mogło by poprawić technologie
    (Chyba lepiej zmienić architekturę na jakąś inną. Można spróbować trzymać dokumenty w zewnętrznej bazie, a w blockchainie jedynie hashe po zmianie[patent Google]. Można również trzymać dokumenty w bazie BigChainDB i zobaczyć jak to się ma do decentralizacji.)


<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" height="40">
<!-- komentarz dr Sobeckiego -->
<!-- Z czego wynika problem który rozwiązujemy
Dlaczego inne rozwiązania go nie rozwiązują np git
Czy znajdziemy literature,  podobnym rozwiązaniem wskazanym
Podobnym problemem
My w pracy planujemy zrobić to I tamyo, bo to możemy zrobić lepiej etc
Dopiero po tym opis blockchaina
Skrócić, dodać odniesienia do literatura
Określić użytkowników, decyzje projektowe
Składa się to z tego i tamtego, wybrano tyle I tyle potwierdzen
Dlaczego tak wybraliśmy, czemu tak jest lepiej I dobrze
We wstępie można powiedzieć gdzie co robi blockchain Ale krótko
Możemy wskazać czym wykazuje się technologia, w czym się przydaje, I co my z tego wykorzystamy do naszego rozwiązania
Rozwiązanie to i to wyko4zystuje I pokazuje taka cechę
Co nam ta cecha da
Medium.com  coward science czy jakieś tam
Wszystkie twierdząc zdania mają być poparte literaturą, a nie tak jak teraz, że same zdania nasze twierdząc
Uzasadnienie
Literatura jako uzupełnienie naszej treści
Każda nasza teoria powinna się kończyć odniesieniem do pracy
Rozdział 3, co wzięliśmy, taka I taka sieć,
A we wnioski to że to był błąd projektowy, trzeba zmienić itd
Diagramy rysunki tabele
Wykresy omówienie wykresów itd
Diagramy klas można dac
Na Litwie w wymianie dokumentów jest blockchain obowiązujący
Jakby ktoś to wrzucił do notatnika czy coś żeby nie zginęło
To będzie kox -->
